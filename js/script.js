//pondit class3 = today we are learnt variable, variable scoping ,scope type .Variable scope is two type - 1.functional variable 2.block variable 
//we have learned more type of variable . such as 
//1.sting number boolean
//2.null undifined  
//3. array object function..
//functional variable is two type such as global variable and local variable..

//now what is functional global variable .functional global variable such that we can use everywhere..


//example here

let a = 10;

console.log(a);   

function local(){
	console.log(a);
}
local();

//now what is local variable .local variable such we can use only it inside the function

//example here

function examplelocal(){
	let b = 20;//this variable only for this function we can't use this variable out of function
	console.log(b)
}
examplelocal();
//console.log(b);//this is error




//second variable scope is block.block variable is two type global and local..

//now what is block global variable .block global variable such that we can use everywhere..

//example here

let c = 30;

console.log(c);   

{
	console.log(c);
}



//now what is block local variable .block local variable such we can use only it inside the block

//example here

{
	let d = 40;//this variable only for this block we can't use this variable out of block
	console.log(d)
}
//console.log(d);//this is error

//1.sting number boolean
//2.null undifined  
//3. array object function..

//sting example

let firstName = "md golam";
let secondName = "rabbi";
let fullName = firstName+" "+secondName;


console.log(fullName);

//number example 

let x = 20;
let y = 10;
let z = x - y;
console.log(`total number is ${z}`);

//boolean example

let num = 5;

if(num < 10){
    console.log(true);
}
else{
    console.log(false);
}

//string to number convert

let str = "253";
let nums = Number(str);
let nums2 = 150;
let result = nums+nums2;
console.log(result);

//number to string convert

let number = 25635;
let strings = String(number);
let number2 = 12546;
let result2 = strings + number2;
console.log(result2);